﻿namespace GsbStockage
{
    partial class FrmConsulterControles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvControles = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvControles)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvControles
            // 
            this.dgvControles.AllowUserToAddRows = false;
            this.dgvControles.AllowUserToDeleteRows = false;
            this.dgvControles.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvControles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvControles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvControles.Location = new System.Drawing.Point(0, 0);
            this.dgvControles.Name = "dgvControles";
            this.dgvControles.ReadOnly = true;
            this.dgvControles.Size = new System.Drawing.Size(555, 265);
            this.dgvControles.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvControles);
            this.panel1.Location = new System.Drawing.Point(42, 127);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(555, 265);
            this.panel1.TabIndex = 1;
            // 
            // FrmConsulterControles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 540);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmConsulterControles";
            this.Text = "Consultation des controles";
            ((System.ComponentModel.ISupportInitialize)(this.dgvControles)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvControles;
        private System.Windows.Forms.Panel panel1;
    }
}