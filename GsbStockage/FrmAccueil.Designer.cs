﻿namespace GsbStockage
{
    partial class FrmAccueil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnConsultationControle = new System.Windows.Forms.Button();
            this.lblFcntCommunes = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(298, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Accueil";
            // 
            // btnConsultationControle
            // 
            this.btnConsultationControle.Location = new System.Drawing.Point(57, 115);
            this.btnConsultationControle.Name = "btnConsultationControle";
            this.btnConsultationControle.Size = new System.Drawing.Size(137, 58);
            this.btnConsultationControle.TabIndex = 1;
            this.btnConsultationControle.Text = "Consultation des controles";
            this.btnConsultationControle.UseVisualStyleBackColor = true;
            this.btnConsultationControle.Click += new System.EventHandler(this.btnConsultationControle_Click);
            // 
            // lblFcntCommunes
            // 
            this.lblFcntCommunes.AutoSize = true;
            this.lblFcntCommunes.Location = new System.Drawing.Point(40, 80);
            this.lblFcntCommunes.Name = "lblFcntCommunes";
            this.lblFcntCommunes.Size = new System.Drawing.Size(176, 16);
            this.lblFcntCommunes.TabIndex = 2;
            this.lblFcntCommunes.Text = "Fonctionnalités communes";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(219, 432);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(222, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Fonctionnalités directeur financier";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(405, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(207, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Fonctionnalités service sécurité";
            // 
            // FrmAccueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 626);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblFcntCommunes);
            this.Controls.Add(this.btnConsultationControle);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmAccueil";
            this.Text = "Acceuil";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnConsultationControle;
        private System.Windows.Forms.Label lblFcntCommunes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}