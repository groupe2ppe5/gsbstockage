﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GsbStockage.BO;
using GsbStockage.BLL;

namespace GsbStockage
{
    public partial class FrmConsulterEntrepriseParNom : Form
    {
        private Label LblChoixEntreprise;
        private ComboBox CboEntreprises;
        private Button BtnFermer;
        private DataGridView dgvCaracEntreprises;
        private GsbStockageDataSet gsbStockageDataSet;
        private BindingSource entrepriseBindingSource;
        private IContainer components;
        private GsbStockageDataSetTableAdapters.EntrepriseTableAdapter entrepriseTableAdapter;
        private DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn nomDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn adresseDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn codePostalDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn villeDataGridViewTextBoxColumn;
        private DataGridViewTextBoxColumn mailDataGridViewTextBoxColumn;
        private Label label1;
        private EntrepriseManager entrepriseManager = new EntrepriseManager();

        public FrmConsulterEntrepriseParNom()
        {
            InitializeComponent();
            CboEntreprises.DataSource = entrepriseManager.GetLesEntreprises();
            CboEntreprises.DisplayMember = "nom";
            CboEntreprises.ValueMember = "nom";
            dgvCaracEntreprises.Hide();
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LblChoixEntreprise = new System.Windows.Forms.Label();
            this.CboEntreprises = new System.Windows.Forms.ComboBox();
            this.BtnFermer = new System.Windows.Forms.Button();
            this.dgvCaracEntreprises = new System.Windows.Forms.DataGridView();
            this.gsbStockageDataSet = new GsbStockage.GsbStockageDataSet();
            this.entrepriseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.entrepriseTableAdapter = new GsbStockage.GsbStockageDataSetTableAdapters.EntrepriseTableAdapter();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nomDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adresseDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codePostalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.villeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaracEntreprises)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbStockageDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.entrepriseBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // LblChoixEntreprise
            // 
            this.LblChoixEntreprise.AutoSize = true;
            this.LblChoixEntreprise.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblChoixEntreprise.Location = new System.Drawing.Point(46, 64);
            this.LblChoixEntreprise.Name = "LblChoixEntreprise";
            this.LblChoixEntreprise.Size = new System.Drawing.Size(159, 21);
            this.LblChoixEntreprise.TabIndex = 0;
            this.LblChoixEntreprise.Text = "Nom de l\'entreprise";
            // 
            // CboEntreprises
            // 
            this.CboEntreprises.FormattingEnabled = true;
            this.CboEntreprises.Location = new System.Drawing.Point(211, 64);
            this.CboEntreprises.Name = "CboEntreprises";
            this.CboEntreprises.Size = new System.Drawing.Size(216, 21);
            this.CboEntreprises.TabIndex = 1;
            // 
            // BtnFermer
            // 
            this.BtnFermer.Location = new System.Drawing.Point(196, 441);
            this.BtnFermer.Name = "BtnFermer";
            this.BtnFermer.Size = new System.Drawing.Size(75, 23);
            this.BtnFermer.TabIndex = 2;
            this.BtnFermer.Text = "Fermer";
            this.BtnFermer.UseVisualStyleBackColor = true;
            this.BtnFermer.Click += new System.EventHandler(this.BtnFermer_Click);
            // 
            // dgvCaracEntreprises
            // 
            this.dgvCaracEntreprises.AllowUserToAddRows = false;
            this.dgvCaracEntreprises.AllowUserToDeleteRows = false;
            this.dgvCaracEntreprises.AllowUserToOrderColumns = true;
            this.dgvCaracEntreprises.AutoGenerateColumns = false;
            this.dgvCaracEntreprises.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvCaracEntreprises.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCaracEntreprises.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.nomDataGridViewTextBoxColumn,
            this.adresseDataGridViewTextBoxColumn,
            this.codePostalDataGridViewTextBoxColumn,
            this.villeDataGridViewTextBoxColumn,
            this.mailDataGridViewTextBoxColumn});
            this.dgvCaracEntreprises.DataSource = this.entrepriseBindingSource;
            this.dgvCaracEntreprises.GridColor = System.Drawing.SystemColors.Control;
            this.dgvCaracEntreprises.Location = new System.Drawing.Point(50, 138);
            this.dgvCaracEntreprises.Name = "dgvCaracEntreprises";
            this.dgvCaracEntreprises.ReadOnly = true;
            this.dgvCaracEntreprises.Size = new System.Drawing.Size(392, 273);
            this.dgvCaracEntreprises.TabIndex = 3;
            this.dgvCaracEntreprises.SelectionChanged += new System.EventHandler(this.dgvCaracEntreprises_SelectionChanged);
            // 
            // gsbStockageDataSet
            // 
            this.gsbStockageDataSet.DataSetName = "GsbStockageDataSet";
            this.gsbStockageDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // entrepriseBindingSource
            // 
            this.entrepriseBindingSource.DataMember = "Entreprise";
            this.entrepriseBindingSource.DataSource = this.gsbStockageDataSet;
            // 
            // entrepriseTableAdapter
            // 
            this.entrepriseTableAdapter.ClearBeforeFill = true;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nomDataGridViewTextBoxColumn
            // 
            this.nomDataGridViewTextBoxColumn.DataPropertyName = "nom";
            this.nomDataGridViewTextBoxColumn.HeaderText = "nom";
            this.nomDataGridViewTextBoxColumn.Name = "nomDataGridViewTextBoxColumn";
            // 
            // adresseDataGridViewTextBoxColumn
            // 
            this.adresseDataGridViewTextBoxColumn.DataPropertyName = "adresse";
            this.adresseDataGridViewTextBoxColumn.HeaderText = "adresse";
            this.adresseDataGridViewTextBoxColumn.Name = "adresseDataGridViewTextBoxColumn";
            // 
            // codePostalDataGridViewTextBoxColumn
            // 
            this.codePostalDataGridViewTextBoxColumn.DataPropertyName = "codePostal";
            this.codePostalDataGridViewTextBoxColumn.HeaderText = "codePostal";
            this.codePostalDataGridViewTextBoxColumn.Name = "codePostalDataGridViewTextBoxColumn";
            // 
            // villeDataGridViewTextBoxColumn
            // 
            this.villeDataGridViewTextBoxColumn.DataPropertyName = "ville";
            this.villeDataGridViewTextBoxColumn.HeaderText = "ville";
            this.villeDataGridViewTextBoxColumn.Name = "villeDataGridViewTextBoxColumn";
            // 
            // mailDataGridViewTextBoxColumn
            // 
            this.mailDataGridViewTextBoxColumn.DataPropertyName = "mail";
            this.mailDataGridViewTextBoxColumn.HeaderText = "mail";
            this.mailDataGridViewTextBoxColumn.Name = "mailDataGridViewTextBoxColumn";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Modern No. 20", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(145, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(221, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Travail sur le DataGridView";
            // 
            // FrmConsulterEntrepriseParNom
            // 
            this.ClientSize = new System.Drawing.Size(506, 476);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvCaracEntreprises);
            this.Controls.Add(this.BtnFermer);
            this.Controls.Add(this.CboEntreprises);
            this.Controls.Add(this.LblChoixEntreprise);
            this.Name = "FrmConsulterEntrepriseParNom";
            this.Text = "Consulter une Entreprise";
            this.Load += new System.EventHandler(this.FrmConsulterEntrepriseParNom_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCaracEntreprises)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gsbStockageDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.entrepriseBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void BtnFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmConsulterEntrepriseParNom_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'gsbStockageDataSet.Entreprise'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.entrepriseTableAdapter.Fill(this.gsbStockageDataSet.Entreprise);

        }

        private void dgvCaracEntreprises_SelectionChanged(object sender, EventArgs e)
        {
            
        }
    }
}
