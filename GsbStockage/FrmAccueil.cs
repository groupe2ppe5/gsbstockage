﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GsbStockage
{
    public partial class FrmAccueil : Form
    {
        public FrmAccueil()
        {
            InitializeComponent();
        }

        private void btnConsultationControle_Click(object sender, EventArgs e)
        {
            FrmConsulterControles consultationControles = new FrmConsulterControles();

            consultationControles.ShowDialog();
        }
    }
}
