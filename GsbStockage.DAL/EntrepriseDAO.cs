﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using GsbStockage.BO;

namespace GsbStockage.DAL
{
    public class EntrepriseDAO
    {
        public List<Entreprise> GetLesEntreprises()
        {
            using (var ctx = new GsbStockageEntities())
            {
                var liste = ctx.Entreprises
                    .ToList();
                return liste;
            }
        }
    }
}
