﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GsbStockage.BO;
using GsbStockage.DAL;
using System.Data;

namespace GsbStockage.DAL
{
    public class ControleDAO
    {
        public List<Controle> GetLesControles()
        {
            using (var ctx = new GsbStockageEntities())
            {
                var liste = ctx.Controles.ToList();
                return liste;
            }
        }
    }
}
