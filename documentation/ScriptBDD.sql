﻿/*------------------------------------------------------------
*        Script SQLSERVER GsbStockage
------------------------------------------------------------*/


/*------------------------------------------------------------
-- Table: ZoneStockage
------------------------------------------------------------*/
CREATE TABLE ZoneStockage(
	id                  INT IDENTITY (1,1) NOT NULL ,
	nom                 VARCHAR (100) NOT NULL ,
	batiment            VARCHAR (100) NOT NULL ,
	etage               VARCHAR (100) NOT NULL ,
	adresse             VARCHAR (100) NOT NULL ,
	ville               VARCHAR (100) NOT NULL ,
	codePostal          VARCHAR (5) NOT NULL ,
	id_CategorieProduit INT  NOT NULL ,
	CONSTRAINT prk_constraint_ZoneStockage PRIMARY KEY NONCLUSTERED (id)
);


/*------------------------------------------------------------
-- Table: Entreprise
------------------------------------------------------------*/
CREATE TABLE Entreprise(
	id         INT IDENTITY (1,1) NOT NULL ,
	nom        VARCHAR (100) NOT NULL ,
	adresse    VARCHAR (100) NOT NULL ,
	codePostal VARCHAR (5) NOT NULL ,
	ville      VARCHAR (100) NOT NULL ,
	mail       VARCHAR (100)  ,
	CONSTRAINT prk_constraint_Entreprise PRIMARY KEY NONCLUSTERED (id)
);


/*------------------------------------------------------------
-- Table: TypeControle
------------------------------------------------------------*/
CREATE TABLE TypeControle(
	id      INT IDENTITY (1,1) NOT NULL ,
	libelle VARCHAR (100) NOT NULL ,
	CONSTRAINT prk_constraint_TypeControle PRIMARY KEY NONCLUSTERED (id)
);


/*------------------------------------------------------------
-- Table: Controle
------------------------------------------------------------*/
CREATE TABLE Controle(
	id              INT IDENTITY (1,1) NOT NULL ,
	dateControle    DATETIME NOT NULL ,
	resumeControle  TEXT  NOT NULL ,
	montantHT       DECIMAL (12,2)  NOT NULL ,
	id_ZoneStockage INT  NOT NULL ,
	id_Entreprise   INT  NOT NULL ,
	CONSTRAINT prk_constraint_Controle PRIMARY KEY NONCLUSTERED (id,id_ZoneStockage)
);


/*------------------------------------------------------------
-- Table: Fonction
------------------------------------------------------------*/
CREATE TABLE Fonction(
	id      INT IDENTITY (1,1) NOT NULL ,
	libelle VARCHAR (100) NOT NULL ,
	CONSTRAINT prk_constraint_Fonction PRIMARY KEY NONCLUSTERED (id)
);


/*------------------------------------------------------------
-- Table: Role
------------------------------------------------------------*/
CREATE TABLE Role(
	id      INT IDENTITY (1,1) NOT NULL ,
	libelle VARCHAR (100) NOT NULL ,
	CONSTRAINT prk_constraint_Role PRIMARY KEY NONCLUSTERED (id)
);


/*------------------------------------------------------------
-- Table: Utilisateur
------------------------------------------------------------*/
CREATE TABLE Utilisateur(
	id         INT IDENTITY (1,1) NOT NULL ,
	login      VARCHAR (100) NOT NULL ,
	motDePasse VARCHAR (100) NOT NULL ,
	nom        VARCHAR (100) NOT NULL ,
	role       VARCHAR (100) NOT NULL ,
	CONSTRAINT prk_constraint_Utilisateur PRIMARY KEY NONCLUSTERED (id)
);


/*------------------------------------------------------------
-- Table: TraceIntervention
------------------------------------------------------------*/
CREATE TABLE TraceIntervention(
	id                    INT IDENTITY (1,1) NOT NULL ,
	tableModifiee         VARCHAR (100) NOT NULL ,
	operation             VARCHAR (100) NOT NULL ,
	dateHeureIntervention DATETIME  NOT NULL ,
	id_Utilisateur        INT  NOT NULL ,
	CONSTRAINT prk_constraint_TraceIntervention PRIMARY KEY NONCLUSTERED (id)
);


/*------------------------------------------------------------
-- Table: CategorieProduit
------------------------------------------------------------*/
CREATE TABLE CategorieProduit(
	id      INT IDENTITY (1,1) NOT NULL ,
	libelle VARCHAR (100) NOT NULL ,
	CONSTRAINT prk_constraint_CategorieProduit PRIMARY KEY NONCLUSTERED (id)
);


/*------------------------------------------------------------
-- Table: Habilitation
------------------------------------------------------------*/
CREATE TABLE Habilitation(
	id              INT  NOT NULL ,
	id_TypeControle INT  NOT NULL ,
	CONSTRAINT prk_constraint_Habilitation PRIMARY KEY NONCLUSTERED (id,id_TypeControle)
);


/*------------------------------------------------------------
-- Table: ZoneStockage_TypeControle
------------------------------------------------------------*/
CREATE TABLE ZoneStockage_TypeControle(
	commentaire     TEXT   ,
	id              INT  NOT NULL ,
	id_ZoneStockage INT  NOT NULL ,
	CONSTRAINT prk_constraint_ZoneStockage_TypeControle PRIMARY KEY NONCLUSTERED (id,id_ZoneStockage)
);


/*------------------------------------------------------------
-- Table: Role_Utilisateur
------------------------------------------------------------*/
CREATE TABLE Role_Utilisateur(
	id             INT  NOT NULL ,
	id_Utilisateur INT  NOT NULL ,
	CONSTRAINT prk_constraint_Role_Utilisateur PRIMARY KEY NONCLUSTERED (id,id_Utilisateur)
);


/*------------------------------------------------------------
-- Table: Autorisation
------------------------------------------------------------*/
CREATE TABLE Autorisation(
	id          INT  NOT NULL ,
	id_Fonction INT  NOT NULL ,
	CONSTRAINT prk_constraint_Autorisation PRIMARY KEY NONCLUSTERED (id,id_Fonction)
);



ALTER TABLE ZoneStockage ADD CONSTRAINT FK_ZoneStockage_id_CategorieProduit FOREIGN KEY (id_CategorieProduit) REFERENCES CategorieProduit(id);
ALTER TABLE Controle ADD CONSTRAINT FK_Controle_id_ZoneStockage FOREIGN KEY (id_ZoneStockage) REFERENCES ZoneStockage(id);
ALTER TABLE Controle ADD CONSTRAINT FK_Controle_id_Entreprise FOREIGN KEY (id_Entreprise) REFERENCES Entreprise(id);
ALTER TABLE TraceIntervention ADD CONSTRAINT FK_TraceIntervention_id_Utilisateur FOREIGN KEY (id_Utilisateur) REFERENCES Utilisateur(id);
ALTER TABLE Habilitation ADD CONSTRAINT FK_Habilitation_id FOREIGN KEY (id) REFERENCES Entreprise(id);
ALTER TABLE Habilitation ADD CONSTRAINT FK_Habilitation_id_TypeControle FOREIGN KEY (id_TypeControle) REFERENCES TypeControle(id);
ALTER TABLE ZoneStockage_TypeControle ADD CONSTRAINT FK_ZoneStockage_TypeControle_id FOREIGN KEY (id) REFERENCES TypeControle(id);
ALTER TABLE ZoneStockage_TypeControle ADD CONSTRAINT FK_ZoneStockage_TypeControle_id_ZoneStockage FOREIGN KEY (id_ZoneStockage) REFERENCES ZoneStockage(id);
ALTER TABLE Role_Utilisateur ADD CONSTRAINT FK_Role_Utilisateur_id FOREIGN KEY (id) REFERENCES Role(id);
ALTER TABLE Role_Utilisateur ADD CONSTRAINT FK_Role_Utilisateur_id_Utilisateur FOREIGN KEY (id_Utilisateur) REFERENCES Utilisateur(id);
ALTER TABLE Autorisation ADD CONSTRAINT FK_Autorisation_id FOREIGN KEY (id) REFERENCES Role(id);
ALTER TABLE Autorisation ADD CONSTRAINT FK_Autorisation_id_Fonction FOREIGN KEY (id_Fonction) REFERENCES Fonction(id);