SET IDENTITY_INSERT [dbo].[CategorieProduit] ON 

INSERT [dbo].[CategorieProduit] ([id], [libelle]) VALUES (1, N'Médicaments classe I (aérosols)')
INSERT [dbo].[CategorieProduit] ([id], [libelle]) VALUES (2, N'Médicaments classe II (solution buvable)')
INSERT [dbo].[CategorieProduit] ([id], [libelle]) VALUES (3, N'Médicaments classe III (poudre)')
INSERT [dbo].[CategorieProduit] ([id], [libelle]) VALUES (4, N'Médicaments classe IV (cachets)')
INSERT [dbo].[CategorieProduit] ([id], [libelle]) VALUES (5, N'Vaccin classe I')
INSERT [dbo].[CategorieProduit] ([id], [libelle]) VALUES (6, N'Vacci classe II')
INSERT [dbo].[CategorieProduit] ([id], [libelle]) VALUES (7, N'Vaccin classe III')
INSERT [dbo].[CategorieProduit] ([id], [libelle]) VALUES (8, N'Gaz médical')
INSERT [dbo].[CategorieProduit] ([id], [libelle]) VALUES (9, N'Composés sanguins')
INSERT [dbo].[CategorieProduit] ([id], [libelle]) VALUES (10, N'Produit radiopharmaceutique')
INSERT [dbo].[CategorieProduit] ([id], [libelle]) VALUES (11, N'Stupéfiants')
INSERT [dbo].[CategorieProduit] ([id], [libelle]) VALUES (12, N'Poudre pour solution type A')
INSERT [dbo].[CategorieProduit] ([id], [libelle]) VALUES (13, N'Poudre pour solution type B')
INSERT [dbo].[CategorieProduit] ([id], [libelle]) VALUES (14, N'Poudre pour solution type C')
SET IDENTITY_INSERT [dbo].[CategorieProduit] OFF