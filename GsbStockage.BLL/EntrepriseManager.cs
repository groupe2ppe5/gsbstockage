﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GsbStockage.DAL;
using GsbStockage.BO;

namespace GsbStockage.BLL
{
    public class EntrepriseManager
    {
        private EntrepriseDAO dal = new EntrepriseDAO();

        public List<Entreprise> GetLesEntreprises()
        {
            return dal.GetLesEntreprises();
        }
    }
}
