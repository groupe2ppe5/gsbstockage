﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GsbStockage.DAL;
using GsbStockage.BO;


namespace GsbStockage.BLL
{
    public class ControleManager
    {
        private ControleDAO dal = new ControleDAO();
        public List<Controle> GetLesControles()
        {
            return dal.GetLesControles();
        }
    }
}
