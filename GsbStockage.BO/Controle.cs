//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GsbStockage.BO
{
    using System;
    using System.Collections.Generic;
    
    public partial class Controle
    {
        public int id { get; set; }
        public System.DateTime dateControle { get; set; }
        public string resumeControle { get; set; }
        public decimal montantHT { get; set; }
        public int id_ZoneStockage { get; set; }
        public int id_Entreprise { get; set; }
    
        public virtual Entreprise Entreprise { get; set; }
        public virtual ZoneStockage ZoneStockage { get; set; }
    }
}
